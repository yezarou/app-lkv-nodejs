'use strict'

var connection = require('../db');

exports.getall = function (req, res) {
    connection.query("SELECT * FROM post_v WHERE thread= ?",[req.params.threadId], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        res.send(result);
    });
}

exports.create = function (req, res) {
    connection.query("CALL NewPost (?, ?, ?)", [req.user, req.params.threadId, req.body.content], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send({
            success: true
        });
    });
}

exports.modify = function (req, res) {
    connection.query("CALL UpdatePost (?, ?, ?)", [req.params.postId, req.user, req.body.content], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send({
            success: true
        });
    });
}

exports.delete = function (req, res) {
    connection.query("CALL DeletePost(?, ?)",[req.params.postId, req.user], (error, result) =>{
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send({
            success: true
        });
    });
}
