'use strict'

var connection = require('../db');

exports.getall = function (req, res) {
    connection.query(
        'SELECT c.id           AS id,\
                chatType       AS type,\
                IF(chatType="INDIVIDUAL",(SELECT image \
                                            FROM user \
                                           WHERE username = (SELECT participant \
                                                               FROM chatParticipant \
                                                              WHERE chat = c.id\
                                                                AND participant <> ?)),image)          AS image,\
                IF(chatType="INDIVIDUAL",(SELECT publicName \
                                            FROM user \
                                           WHERE username = (SELECT participant \
                                                               FROM chatParticipant \
                                                              WHERE chat = c.id\
                                                                AND participant <> ?)),title)          AS title,\
                m.content       AS last_message,\
                m.creationDate  AS last_message_date,\
                IF(n.message IS null OR n.username != ?, TRUE, FALSE) AS read_message\
           FROM chat c\
           JOIN message m ON c.id = m.chat\
           JOIN chatParticipant p ON c.id = p.chat\
      LEFT JOIN notification n ON m.id = n.message\
          WHERE m.active = true\
            AND p.participant = ?\
            AND m.id = (SELECT max(m1.id) FROM message m1 WHERE m1.active=true AND m1.chat = c.id)', [req.user, req.user, req.user, req.user],
    (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error
            });
        }
        res.send(result);
    });
}

exports.create = function (req, res) {
    console.log(req);
    connection.query("CALL NewIndividualChat (?, ?, @chatid); SELECT @chatid AS id", [req.user, req.body.receiver], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send(result[1][0]);
    });
}
