'use strict'

var connection = require('../db');

exports.getall = function (req, res) {
    connection.query(
        'SELECT *\
           FROM message_v\
          WHERE chat = ?\
          ORDER BY creation_date;\
         DELETE n\
           FROM notification n\
           JOIN message m ON n.message = m.id\
          WHERE n.username=?\
            AND m.chat=?',[req.params.chatid, req.user, req.params.chatid],
    (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error
            });
        }
        res.send(result[0]);
    });
}

exports.getnew = function (req, res) {
    connection.query(
        ' SELECT m.id           AS id,\
                 m.author       AS username,\
                 u.publicName   AS name,\
                 m.content      AS content,\
                 u.image        AS image,\
                 m.creationDate AS creation_date,\
                 m.chat         AS chat\
            FROM message m\
      RIGHT JOIN notification n ON m.id = n.message\
            JOIN user u ON m.author = u.username\
           WHERE m.active = 1\
             AND n.sended = 0\
             AND n.username = ?;\
          UPDATE notification\
             SET sended = 1\
           WHERE username = ?;', [req.user, req.user],
    (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error
            });
        }
        res.send(result[0]);
    });
}

exports.create = function (req, res) {
    connection.query("CALL NewMessage (?, ?, ?)", [req.user, req.params.chatid, req.body.content], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send({
            success: true
        });
    });
}

exports.deleteNotification = function(req, res) {
    connection.query('DELETE FROM notification WHERE username = ? AND message = ?;', [req.user, req.body.message], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send({
            success: true
        });
    });
}
