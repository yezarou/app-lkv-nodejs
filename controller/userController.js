'use strict'

var connection = require('../db');

exports.search = function (req, res) {
    var filter = '';
    if (req.query.name !== undefined) {
        filter = ' WHERE upper(username) LIKE upper("%'+req.query.name+'%")';
    }
    connection.query("SELECT username AS username, publicName AS public_name, image AS image, isAdmin as is_admin FROM user" + filter, (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send(result);
    });
}

exports.getProfile = function (req, res) {
    connection.query('SELECT username AS username, email AS email, birth AS birth, publicName AS public_name, image AS image, isAdmin AS admin, creationDate AS creation_date FROM user u WHERE username = ?',[req.user], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send(result[0]);
    });
}

exports.uploadImage = function (req, res) {
    var base64Data = req.body.image.replace("^data:image\/png;base64,/", "");
    require("fs").writeFile("./public/images/" + req.user + ".png", base64Data, 'base64', function(err) {
	console.log(err);
        if (err){
            return res.status(400).send({
                success: false,
                message: err
            });
        }
        return res.status(200).send({
            success: true
        });
    });
}

exports.changePublicName = function (req, res) {
    connection.query('UPDATE user SET publicName = ? WHERE username = ?', [req.body.publicname, req.user], (error, result) => { 
        if (error) {
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.status(200).send({
            success: true
        });
    });
}

exports.changePassword = function (req, res) {
    connection.query('UPDATE userCredentials SET password = ? WHERE username = ?', [req.body.password, req.user], (error, result) => { 
        if (error) {
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.status(200).send({
            success: true
        });
    });
}

