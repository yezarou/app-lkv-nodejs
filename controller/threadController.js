'use strict'

var connection = require('../db');

exports.getall = function (req, res) {
    connection.query("SELECT * FROM thread_v ORDER BY lastpost_creation_date DESC", (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        res.send(result);
    });
}

exports.create = function (req, res) {
    connection.query("CALL NewThread (?, ?, ?, ?)", [req.user, req.body.category, req.body.title, req.body.content], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send({
            success: true
        });
    });
}

exports.modify = function (req, res) {
    connection.query("CALL UpdateThread (?, ?, ?, ?, ?)", [req.params.threadId, req.user, req.body.title, req.body.content, req.body.category], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send({
            success: true
        });
    });
}

exports.delete = function (req, res) {
    console.log('delete');
    connection.query("CALL DeleteThread(?, ?)",[req.params.threadId, req.user], (error, result) =>{
        console.log(error);
        if (error){
            return res.status(400).send({
                success: req.user,
                message: error.sqlMessage
            });
        }
        return res.send({
            success: true
        });
    });
}
