'use strict'

var randomstring = require('randomstring');
var connection = require('../db');
var nodemailer = require('nodemailer');
var fs = require('fs');
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'lethalknightventure@gmail.com',
        pass: 'imhphwzpteciqkcc'
    }
});

exports.validate = function(req, res, next) {
    connection.query("SELECT * FROM auth WHERE token=?",[req.header('Authorization')], (error, result) => {        
        try{
            if (result.length > 0) {
                req.user = result[0].username;
                return next();
            }
            throw 'Inexistent token.';
        } catch(err) {
            return res.status(401).send({
                success: false,
                message: err
            });
        }
    });
 }

 exports.login = function(req, res) {
    connection.query("CALL Login(?, ?, @token);SELECT a.username AS username, a.token AS token, u.isAdmin AS admin FROM auth a JOIN user u ON u.username = a.username WHERE token = @token;",[req.body.username, req.body.password], (error, result) => {        
        if (error){
            return res.status(400).send({
                success: false,
                message: error.sqlMessage
            });
        }
        return res.send(result[1][0]);
    });   
 }

 exports.register = function(req, res) {
    connection.query('CALL Register(?, ?, ?, ?)',[req.body.username, req.body.password, req.body.email, req.body.birthdate], (error, result) => {
        if (error){
            return res.status(400).send({
                success: false,
                message: 'Unable to register. Try it again.'
            });
        }
        fs.copyFile('./public/images/default', './public/images/'+req.body.username+'.png', (err)=>{console.log(err)});
	connection.query('CALL NewIndividualChat(?, ?, @chat);CALL NewMessage(?, @chat, ?, @id)', ['Ruben', req.body.username, 'Ruben', '¡Buenas! Gracias por registrarte. Soy el creador de la aplicación, si necesitas cualquier cosa, no dudes en usar el chat.'], (error, result) => {});
        return res.status(200).send({
            success: true,
            message: 'Registered account! Remember to validate your e-mail.'
        });
    });
 }

 exports.disconnect = function(req, res) {
    connection.query("DELETE FROM auth WHERE token = ?",[req.header('Authorization')], (error, result)=>{
        if (error){
            return res.status(400).send({
                success: false,
                message: 'Unable to disconnect. Try it again.'
            });
        }
        return res.status(200).send({
            success: true,
            message: 'Disconnected!'
        });
     });
 }

 exports.disconnectAll = function(req, res) {
    connection.query("DELETE FROM auth WHERE username = ? AND token <> ?",[req.user, req.header('Authorization')], (error, result)=>{
       if (error){
           return res.status(400).send({
               success: false,
               message: 'Unable to disconnect. Try it again.'
           });
       }
       return res.status(200).send({
           success: true,
           message: 'Disconnected!'
       });
    });
}

exports.sendVerificationEmail = function(req, res) {
    var id = randomstring.generate(16);
    connection.query("CALL SendVerification((SELECT username FROM user WHERE email = ?), ?)", [req.body.email, id], (error, result) => {
        if (!error) {
            const mailOptions = {
                from: 'lethalknightventure@gmail.com',
                to: req.body.email,
                subject: '[LKV team] Verify your account',
                html: '<html><p>For verify your new account, access to the following url:</p><p>https://yezarou.edufdezsoy.es/api/auth/verificate?id='+id+'</p><p> if you didn\'t ask to verify this address, you can ignore this email.</p><p> Thanks,</p><p>LethalKnightVenture Team.</p></html>',
                text: 'For verify your new account, access to the following url: https://yezarou.edufdezsoy.es/api/auth/verificate?id='+id+' if you didn\'t ask to verify this address you can ignore this email. Thanks, LethalKnightVenture Team.'
            }

            transporter.sendMail(mailOptions, function(err, info) {
                return res.status(200).send({
                    success: true,
                    message: 'Email sended'
                });
            });
        }
        return res.status(400).send({
            success: false,
            message: 'Unable to send the email, try again'
        });
    });
 }
 exports.verificate = function(req, res) {
    connection.query('CALL VERIFICATE(?)',[req.query.id], (error, result) => {
        if (!error) {
           res.send('<h1>Account verified!</h1>');
        }
        return res.send('<h1>Unable to verify that account</h1>');
     });
 }
