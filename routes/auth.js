'use strict'

var express = require('express');
var router = express.Router();
var authcontroller = require('../controller/authController');

router.post('/login/', authcontroller.login);
router.post("/register", authcontroller.register);
router.get("/disconnect", authcontroller.disconnect);
router.get("/disconnectsessions", authcontroller.validate, authcontroller.disconnectAll);
router.post("/sendverification", authcontroller.sendVerificationEmail);
router.get("/verificate", authcontroller.verificate);

module.exports = router;
