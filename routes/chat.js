'use strict'

var express = require('express');
var router = express.Router();
var authcontroller = require('../controller/authController');
var chatcontroller = require('../controller/chatController');
var messagecontroller = require('../controller/messageController');

router.use('/', authcontroller.validate);

router.get('/', chatcontroller.getall);
router.post('/new', chatcontroller.create);

router.get('/:chatid/messages',messagecontroller.getall);
router.get('/newmessages', messagecontroller.getnew);
router.post('/:chatid/messages/new',messagecontroller.create);
router.post('/deleteNotification',messagecontroller.deleteNotification);
module.exports = router;
