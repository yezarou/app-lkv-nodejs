'use strict'

var express = require('express');
var router = express.Router();
var authcontroller = require('../controller/authController');
var usercontroller = require('../controller/userController');

router.use('/search', usercontroller.search);
router.get('/profile', authcontroller.validate, usercontroller.getProfile);
router.post('/image/upload', authcontroller.validate, usercontroller.uploadImage);
router.post("/changeName", authcontroller.validate, usercontroller.changePublicName);
router.post("/changePassword", authcontroller.validate, usercontroller.changePassword);

module.exports = router;
