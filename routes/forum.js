'use strict'

var express = require('express');
var router = express.Router();
var authcontroller = require('../controller/authController');
var threadcontroller = require('../controller/threadController');
var postcontroller = require('../controller/postController');

router.get('/', threadcontroller.getall);
router.post('/new', authcontroller.validate, threadcontroller.create);
router.put("/modify/:threadId", authcontroller.validate, threadcontroller.modify);
router.delete("/delete/:threadId", authcontroller.validate, threadcontroller.delete)

router.get('/:threadId/posts/', postcontroller.getall);
router.post('/:threadId/posts/new', authcontroller.validate, postcontroller.create);
router.put("/posts/modify/:postId", authcontroller.validate, postcontroller.modify);
router.delete("/posts/delete/:postId", authcontroller.validate, postcontroller.delete);

module.exports = router;