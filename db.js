'user strict';

var mysql = require('mysql');

//local mysql db connection
var connection = mysql.createConnection({
    host                : 'localhost',
    user                : 'lkvapi',
    password            : 'lkv23',
    charset             : 'utf8mb4',
    database            : 'lkv_db',
    multipleStatements  : true
});

module.exports = connection;
