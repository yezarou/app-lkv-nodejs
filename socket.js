'use strict'

var connection = require('./db');

module.exports = function(server){

    var io = require('socket.io')(server);
    const chatConnect = io.of('api/socket/chat');

    chatConnect.on('connection', function(socket){
        var roomName, json, username;
        socket.on('join', function(room){
            json = JSON.parse(room);
            roomName = json.room;
            socket.join(roomName);
            username = json.username;
        });
        socket.on('message', function(body){
            json = JSON.parse(body);
	    connection.query('CALL NewMessage(?, ?, ?, @lastmessage); SELECT * FROM message_v WHERE id=@lastmessage;', [json.username, roomName, json.message], (error, result) => {
                if (!error){
                    socket.broadcast.to(roomName).emit('newMessage', result[1][0]);
                    socket.emit('newMessage', result[1][0]);
                }
            });
        });
        socket.on('delete', function(body){
            connection.query('UPDATE message SET active = false WHERE id = ?;', [body, body], (error, result) => {
                if (!error){
                    socket.broadcast.to(roomName).emit('deleteMessage', body);
                    socket.emit('deleteMessage', body);
                }
            });
        });
    });
}
