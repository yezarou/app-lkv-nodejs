var fs = require('fs');

const privateKey = fs.readFileSync('/etc/letsencrypt/live/yezarou.edufdezsoy.es/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/yezarou.edufdezsoy.es/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/yezarou.edufdezsoy.es/chain.pem', 'utf8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};


var express = require('express'),
https = require('https'),
app = express(),
port = process.env.PORT || 3000,
router = express.Router(),
bodyParser = require('body-parser'),
server = https.createServer(credentials, app).listen(port);

app.use(router);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({extended:true, limit:'50mb'}));

router.get('/api', function(req, res) {
   res.send("LethalKnightVenture's API is available!");
});
var auth = require('./routes/auth'),
forum = require('./routes/forum'),
chat = require('./routes/chat'),
user = require('./routes/user');

app.use('/api/auth', auth);
app.use('/api/thread', forum);
app.use('/api/chat', chat);
app.use('/api/user', user);

require('./socket')(server);

console.log('RESTFUL API server started on: ' + port);
